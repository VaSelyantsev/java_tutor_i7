package part_2.lesson_1.order_task.service;



import part_2.lesson_1.order_task.entities.Item;
import part_2.lesson_1.order_task.entities.Order;
import part_2.lesson_1.order_task.entities.Status;
import part_2.lesson_1.order_task.entities.StatusGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

//Сервис для работы с заказами. Менять интерфейс запрещено!
public interface OrderService {

	//Получить заказ по айди, либо null если такого заказа не найдено
	Order getOrderById(List<Order> orders, Long id);

	//Получить список заказов в определенном статусе, либо пустой список если таких нет
	List<Order> getAllOrdersByStatus(List<Order> orders, Status status);

	//Получить список заказов со статусом определенной группы, либо пустой список если таких нет
	List<Order> getAllOrdersByStatusGroup(List<Order> orders, StatusGroup statusGroup);

	//Получить список заказов без удаленных позиций товаров, либо пустой список если таких нет
	List<Order> getAllOrdersWithoutDeletedItems(List<Order> orders);

	//Получить заказы, сумма которых больше заданной, либо пустой список если таких нет.
	// Сумма заказов рассчитывается по следующей формуле:
	//Сумма произведений цены каждой позиции на ее количество (удаленные позиции не учитываются), с учетом скидки (в процентах)
	List<Order> getOrdersWithPriceGreaterThan(List<Order> orders, Double price);



	// 1. Превратить в мапу айди заказа - заказ
	Map<Long,Order> idAndOrderMap (Stream<Order> orderStream);


	//2.Посчитать сумму стоимости всех заказов (Учитывая процент скидки!), используйте метод reduce()
	Double calculateTheSum(List<Order> orders);


	//3.Собрать мапу Итем - лист заказов, в которых он есть (используйте функции группировки)


	Map<Item, List<Order>> itemAndListOfOrders(List<Order> orders);


}
