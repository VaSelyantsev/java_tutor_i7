package part_1.lesson_7;

public class RemoteControl {

    public String turnSomethingOn(IkPortCompatible item){
        return item.turnOn();
    }

    public String turnSomethingOff(IkPortCompatible item){
        return item.turnOff();
    }
}
