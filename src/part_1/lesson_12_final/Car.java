package part_1.lesson_12_final;

public class Car {
    public static void main(String[] args) {
        Car c = new Car();

        Car bmw = new BMWx5();
        c.launch();
        c.launch("hi!");

        bmw.launch();
    }

    public void launch(){
        System.out.println("starting...");
    }
    public void launch(String msg){
        System.out.println(msg);
        System.out.println("starting...");
    }
}
class BMWx5 extends Car{
    /*
    @Override
    public void launch() {
        System.out.println("BMW x5 starting...");
    }*/
}