package part_1.lesson_6.animals;

public abstract class Feline extends Animal {
    @Override
    void roam() {
        System.out.println("Хожу по кошачьи");
    }
}
