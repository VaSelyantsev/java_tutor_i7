package part_1.lesson_6.animals;

public abstract class Canine extends Animal {
    @Override
    void roam() {
        System.out.println("Хожу по собачьи");
    }
}
