package part_1.lesson_9.lesson.src;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        System.out.println(task2(Arrays.asList(1,2,2,1,-3,7,7,7,7,8,9,9,8)));
/*        List<Integer> list = new ArrayList<>();
//      [1,null,...] - capacity = 10; size = 0 -> 1
        list.add(7);
        System.out.println(list); // [7]
        System.out.println(list.contains(7));
        list.remove(7);
        list.get(index);
        list.add(3,8);
        Integer[] objects = (Integer[]) list.toArray();

        Integer[] arr = new Integer[5];
        List<Integer> newList = Arrays.asList(arr);

        int x = Integer.parseInt("777");
        // Autoboxing и Unboxing
        int y = new Integer(7); // Unboxing
        Integer integer = 5; // Autoboxing*/
    }

    public static List<Integer> createNewList(List<Integer> list) {
        List<Integer> result = new ArrayList<>(list.size());
        for (int i = list.size() - 1; i >= 0; i--) {
            if (list.get(i) > 0) {
                result.add(list.get(i));
            }
        }
        return list;
    }

    public static List<Integer> task2(List<Integer> list) {
        List<Integer> result = new ArrayList<>(list.size());
        for (int i = 0; i < list.size() - 1; i++) {
            result.add(list.get(i));
            if (list.get(i).equals(list.get(i + 1))) {
                result.remove(list.get(i));
            }
        }
        return result;
    }





//  Ранее
    public void a() {

    }

    public static void b() {
//        a();

/*        Solution s = new Solution();
          s.a();*/
    }
}
/*
class A{
    static void m(){}
}
class B extends A{

    static void m(){}
}*/
