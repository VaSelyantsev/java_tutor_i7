package part_1.lesson_9.homeTheatre.src.components;


public class Screen {
    public void up() {
        System.out.println("Theater Screen going up");
    }

    public void down() {
        System.out.println("Theater Screen going down");
    }
}
