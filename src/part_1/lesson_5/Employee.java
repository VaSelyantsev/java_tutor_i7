package part_1.lesson_5;

public class Employee extends Human{
    private String inn;
    private int salary;
    private boolean hasAJob;
    private String uniqueId;

    public void setUniqueId(String id){
        uniqueId = id;
    }

    public void setUniqueId(int number){
        uniqueId = "" + number;
    }


    public void foo(){
        super.showInfo();
    }

    public void toRaise(){
        System.out.println("Employee " + getFirstName() + "  promoted");
    }



    public Employee(){}

    @Override
    public void showInfo(){
        System.out.println("I'm employee, my name is " + getFirstName());
    }

 /*   public Employee(String firstName, String lastName, int age, String inn, int salary){
        super(firstName, lastName, age);
        this.inn = inn;
        this.salary = salary;
    }*/

    public void toHire(){
        hasAJob = true;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public boolean isHasAJob() {
        return hasAJob;
    }

    public void setHasAJob(boolean hasAJob) {
        this.hasAJob = hasAJob;
    }
}
