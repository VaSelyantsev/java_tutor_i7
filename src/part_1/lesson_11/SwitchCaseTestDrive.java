package part_1.lesson_11;

import java.util.Scanner;

public class SwitchCaseTestDrive {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
//        String s = sc.nextLine();

/*        switch (s) {
            case "Hello" -> System.out.println("Hello, how are you?");
            case "Bye" -> System.out.println("Bye bye");
            default -> System.out.println("I don't understand you");
        }*/

        int x = sc.nextInt();

        // int y = 5;
        int y = switch (x){
            case 1 ->  10;
            case 2 -> 20;
            case 3 -> 30;
            default -> -1;
        };
        System.out.println(y);
    }
}
